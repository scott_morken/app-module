<section class="section">
    <div class="container">
        <div class="alert alert-error">
            <?php foreach ($errors as $error):
                echo $error;
            endforeach; ?>
        </div>
    </div>
</section>