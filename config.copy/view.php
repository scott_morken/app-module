<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:59 AM
 */
return array(
    /*
    |--------------------------------------------------------------------------
    | View Provider
    |--------------------------------------------------------------------------
    |
    | The View provider to return from any requests to the view service provider
    | The default is PhpView which is a simple handler capable of injecting
    | variables into the view during rendering
    |
    */
    'provider' => 'Smorken\View\PhpView',
    /*
    |--------------------------------------------------------------------------
    | Loader Options
    |--------------------------------------------------------------------------
    |
    | Array of options to pass in when creating the view provider.
    | In this case, we're setting up the directory where the view files will
    | exist.
    |
    */
    'loaderopts' => array(\Smorken\Utils\PathUtils::base() . '/views'),
    /*
    |--------------------------------------------------------------------------
    | Master view file
    |--------------------------------------------------------------------------
    |
    | If set, this will provide a template that any views you call will be injected
    | into.  It will need to have a <?php echo $content; ?> entry to show the sub-view
    |
    */
    'master' => 'master',
);
