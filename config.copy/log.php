<?php

return array(
    'loggers' => array(
        'errors' => array(
            'handlers' => array(
                'Monolog\Handler\StreamHandler' => array(
                    \Smorken\Utils\PathUtils::base() . '/logs/errors.log',
                    Monolog\Logger::WARNING,
                ),
                /*'Monolog\Handler\NativeMailerHandler' => array(
                    'my.name@mydomain.edu',
                    'Exception [My App]',
                    'exception@myapp.mydomain.edu',
                    Monolog\Logger::ERROR,
                ),*/
            ),
        ),
    )
);
