<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:59 AM
 */
return array(
    'provider' => 'Smorken\View\PhpView',
    'loaderopts' => array(\Smorken\Utils\PathUtils::base() . '/views'),
);