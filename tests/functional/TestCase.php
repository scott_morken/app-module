<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:18 AM
 */


class TestCase extends PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Application\App
     */
    protected $app;

    public function setUp()
    {
        $container = new Pimple\Container();
        $app = \Smorken\Application\App::getInstance($container);
        $paths = require __DIR__ . '/paths.php';
        $app->addPaths($paths);
        $this->app = $app;
    }

    public function tearDown()
    {
        \Smorken\Application\App::destroy();
    }

}