<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 10:59 AM
 */

require_once __DIR__ . '/TestCase.php';

class ConfigServiceTest extends TestCase {

    protected $cls = 'Smorken\Config\ConfigService';

    public function testRegisterServiceGetItem()
    {
        $this->app->registerService($this->cls);
        $this->assertEquals('bar', $this->app['config']['app']['foo']);
    }

    public function testRegisterServiceGetItemDotted()
    {
        $this->app->registerService($this->cls);
        $this->assertEquals('bar', $this->app['config']['app.foo']);
    }

    public function testRegisterServiceGetWithGet()
    {
        $this->app->registerService($this->cls);
        $this->assertEquals('bar', $this->app['config']->get('app.foo'));
    }
} 