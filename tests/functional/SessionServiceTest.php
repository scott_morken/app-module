<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:44 PM
 */

require_once __DIR__ . '/TestCase.php';

class SessionServiceTest extends TestCase {

    public function setUp()
    {
        parent::setUp();
        $this->app->start();
    }

    public function testSessionRegistered()
    {
        $this->assertArrayHasKey('session', $this->app->getServices());
    }

    public function testCanWriteToSession()
    {
        $this->app['session']->foo = 'bar';
        $this->assertEquals('bar', $this->app['session']->foo);
    }
} 