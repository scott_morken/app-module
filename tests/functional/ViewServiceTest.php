<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:08 PM
 */

require_once __DIR__ . '/TestCase.php';

class ViewServiceTest extends TestCase {

    public function setUp()
    {
        parent::setUp();
        $this->app->start();
    }

    public function testViewRegistered()
    {
        $this->assertArrayHasKey('view', $this->app->getServices());
    }

    public function testViewNoMaster()
    {
        $view = $this->app['view'];
        $o = $view->render('test', array(), false);
        $this->assertEquals('test content', $o);
    }

    public function testViewNoMasterWithVariable()
    {
        $view = $this->app['view'];
        $o = $view->render('test_variable', array('content' => 'content'), false);
        $this->assertEquals('test content', $o);
    }

    public function testViewMaster()
    {
        $view = $this->app['view'];
        $view->setMaster('master');
        $o = $view->render('test', array(), false);
        $expected = "master\ntest content\nmaster";
        $this->assertEquals($expected, $o);
    }

    public function testViewMasterWithVariable()
    {
        $view = $this->app['view'];
        $view->setMaster('master');
        $o = $view->render('test_variable', array('content' => 'content'), false);
        $expected = "master\ntest content\nmaster";
        $this->assertEquals($expected, $o);
    }

    public function testViewMasterWithVariableInBoth()
    {
        $view = $this->app['view'];
        $view->setMaster('master');
        $o = $view->render('test_variable', array('content' => 'content', 'master' => 'master'), false);
        $expected = "master\ntest content\nmaster";
        $this->assertEquals($expected, $o);
    }
} 