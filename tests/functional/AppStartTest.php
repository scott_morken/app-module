<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:41 AM
 */

require_once __DIR__ . '/TestCase.php';

class AppStartTest extends TestCase {

    public function testStart()
    {
        $this->app->start();
        $this->assertArrayHasKey('loggers', $this->app->getServices());
    }
} 