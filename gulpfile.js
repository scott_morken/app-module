var gulp = require('gulp');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var gutil = require('gulp-util');
var rename = require('gulp-rename');

gulp.task('default', ['js', 'copy', 'less']);

gulp.task('js', function() {
});

gulp.task('copy', function() {
    gulp.src('./assets/js/*.js')
        .pipe(gulp.dest('./public/assets/js'));
    gulp.src('./bower_components/bootstrap/fonts/*')
        .pipe(gulp.dest('./public/assets/fonts'));
    gulp.src('./assets/images/*')
        .pipe(gulp.dest('./public/assets/images'));
});

gulp.task('less', function() {
    gulp.src('./assets/less/app.less')
        .pipe(less())
        .pipe(gulp.dest('./public/assets/css'))
        .pipe(minifyCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('./public/assets/css'))
        .on('error', gutil.log);
});
