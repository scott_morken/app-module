<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/23/14
 * Time: 11:09 AM
 */

/**
 * Variable help
 * @var Smorken\Application\App $app
 * @var Smorken\Router\RouteCollection $routes
 */

$routes = $app['routes'];

/**
 * Shows the front page for the app
 * Renders views/index.php with the injected variable $myvar
 * GET request for base route (http://foo.com/myapp)
 */
$routes->get('', function ($request) use ($app) {
    /**
     * @var Smorken\View\ViewInterface $view
     */
    $view = $app['view'];
    $view->render('index', ['myvar' => date('Y-m-d H:i:s')]);
});
/**
 * Same route as above.  My router isn't very smart though and I haven't put the work in to make
 * sure both match the same route (/ or /index.php)
 * GET request for base route + front controller aka index.php (http://foo.com/myapp/index.php)
 */
$routes->get('index.php', function ($request) use ($app) {
    $app['view']->render('index', ['myvar' => date('Y-m-d H:i:s')]);
});

/**
 * Shows the login page
 * Renders views/login.php
 * GET request for login route (http://foo.com/myapp/login or http://foo.com/myapp/index.php/login)
 */
$routes->get('login', function ($request) use ($app) {
    $app['view']->render('login');
});//->before('filter.https', $auth);
/**
 * Posts the login information from the user to the server and handles it.
 * POST request for login route (result of submitting login form)
 */
$routes->post('login', function ($request) use ($app) {
    /**
     * @var Smorken\Auth\AuthHandler $auth
     */
    $auth = $app['auth'];
    $authenticated = false;
    if (isset($_POST['username']) && isset($_POST['password'])) {
        // Turns the super global $_POST into a sanitized variable $post
        $post = filter_input_array(INPUT_POST, [
            'username' => FILTER_SANITIZE_STRING,
            'password' => FILTER_SANITIZE_STRING,
        ]);
        // Authenticates the user, if successful and config/auth.php - redirect is true, automatically
        // redirects to the $post['redirectTo'] url
        $authenticated = $auth->authenticate($post['username'], $post['password']);
    }
    if (!$authenticated) {
        // Authentication failed, render the views/login.php page view and pass along the errors as
        // injected variables ($errors = array of errors)
        $app['view']->render('login', ['errors' => $auth->getErrors()]);
    } else {
        // Authenticated, send the user back to where they originally wanted to go
        Smorken\Utils\Redirect::go($auth->getReturnUrl());
    }
});//->before('filter.https', $auth);
/**
 * Does the logout and redirects to the front page
 * GET request for logout route (http://foo.com/myapp/logout or http://foo.com/myapp/index.php/logout)
 */
$routes->get('logout', function ($request) use ($app) {
    // Calls the logout method of Smorken\Auth\AuthHandler
    $app['auth']->logout();
    // Creates a url based on the base route of the application (front page)
    $url = $app['url']->to('');
    // Go go gadget redirect to the front page!
    \Smorken\Utils\Redirect::go($url);
});

/**
 * This route is protected (login required)
 * See filters.php
 * GET request for protected route (http://foo.com/myapp/protected or http://foo.com/myapp/index.php/protected)
 */
$routes->get('protected', function ($request) use ($app) {
    $app['view']->render('protected');
})->before('filter.auth', $app);
