<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:02 AM
 */

/**
 * Pimple is our dependency injection container
 */
$container = new Pimple\Container();
/**
 * We create our app handler here
 * Yes, Pimple is Pimple and not a contract, bad juju but it's ok
 */
$app = Smorken\Application\App::getInstance($container);

/**
 * Adds the array from bootstrap/paths.php to $app->addPaths()
 */
$app->addPaths(require __DIR__ . '/paths.php');

/**
 * Add the filters so that they are available immediately
 */
require 'filters.php';

/**
 * Starts up the app
 * This simply registers the core services and any additional
 * services that are in config/app.php - services
 */
$app->start();

/**
 * Defines the DEBUG constant, uses the config provider to lookup app.debug
 * in the default case, this will look at config/app.php - debug
 */
define('DEBUG', $app['config']['app.debug']);

/**
 * This provides our router, which implements a contract of RouterInterface
 * In this case, the concrete router object is really Smorken\Router\LegacyRouter
 * @var Smorken\Router\RouterInterface $router
 */
$router = $app['router'];

/**
 * If the service is loaded, let's use it!  It is optional so if it's not loaded, just continue.
 */
if ($router) {
    /**
     * Load the bootstrap/routes.php file so that the route collection has routes.
     */
    require_once 'routes.php';
    /**
     * Dispatch our route - this does the matching and runs whatever the matching route
     * requests.
     */
    $router->dispatch($_SERVER['REQUEST_URI']);
}

/**
 * Give our $app back to the index.php front controller
 */
return $app;
