<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 12/1/14
 * Time: 8:08 AM
 */

$app['filter.auth'] = function ($request, $response) use ($app) {
    if (!$app['auth']->isAuthenticated()) {
        $url = $app['url']->to($app['config']->get('auth.login_route', 'login'));
        $app['auth']->setReturnUrl($_SERVER['PHP_SELF']);
        Smorken\Utils\Redirect::go($url);
    }
};

$app['filter.https'] = function ($request, $response) use ($app) {
    if (!Smorken\Utils\Url::isSecure($_SERVER)) {
        $url = Smorken\Utils\Url::full($_SERVER, true);
        Smorken\Utils\Redirect::go($url);
    }
};
