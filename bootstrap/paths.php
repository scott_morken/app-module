<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Public Path
    |--------------------------------------------------------------------------
    |
    | The public path contains the assets for your web application, such as
    | your JavaScript and CSS files, and also contains the primary entry
    | point for web requests into these applications from the outside.
    |
    | Provides Smorken\Utils\PathUtils::get('public')
    */

    'public' => __DIR__ . '/../public',

    /*
    |--------------------------------------------------------------------------
    | Base Path
    |--------------------------------------------------------------------------
    |
    | The base path is the root of the installation. Most likely you
    | will not need to change this value. But, if for some wild reason it
    | is necessary you will do so here, just proceed with some caution.
    |
    | Provides Smorken\Utils\PathUtils::base() or
    | Smorken\Utils\PathUtils::get('base') or
    | base_path()
    */

    'base' => __DIR__ . '/..',

];
