<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:04 AM
 */

/**
 * Set up our autoloader
 */
require __DIR__.'/../bootstrap/autoload.php';

/**
 * Load the boostrap/start.php file which does most of the magic to make this
 * thing work.
 * @var Smorken\Application\App $app
 */
$app = require_once __DIR__.'/../bootstrap/start.php';
