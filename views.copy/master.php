<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My App</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <?php echo $content; ?>
</div> <!-- /container -->
<footer id="footer">
    <div class="container text-sm-center">
        <div class="text-muted"><small>&copy; <?php echo date('Y'); ?> Phoenix College</small></div>
        <div>
            <a href="https://www.maricopa.edu" title="Maricopa Community Colleges">
                <img height="30" src="<?php echo asset('images/DO-HC-30.png'); ?>" alt="A Maricopa Community College"/>
            </a>
        </div>
    </div>
</footer>
</body>
</html>