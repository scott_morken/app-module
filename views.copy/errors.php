<section class="section">
    <div class="container">
        <div class="alert alert-error">
            <?php foreach ($errors as $error):
                e($error);
            endforeach; ?>
        </div>
    </div>
</section>
