## Simple Application Framework

This application is licensed under the [MIT License](http://opensource.org/licenses/MIT).

### Use

Heavily borrowed from Laravel 4 (and by extension Symfony 2).

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.4+
* [Composer](https://getcomposer.org/)
* [Node.js](http://nodejs.org/) and [Node Package Manager](https://www.npmjs.org/)
* [Bower](http://bower.io) (can be installed globally via npm: $ sudo npm -g install bower)

Bower gotcha on **Ubuntu** (node installed via package manager): Install will fail with an error 127, 
can't find file: /usr/bin/env: node

```
$ sudo ln -s /usr/bin/nodejs /usr/bin/node
```

### Installation

* Clone the repo

```
$ git clone git@bitbucket.org:scott_morken/app-module.git my_project
```

Install dependencies via [composer](https://getcomposer.org/doc/00-intro.md)

```
$ composer install
```

Install asset dependencies via [bower](https://bower.io)

```
$ bower install
$ gulp #handle asset setup
```

Create (or copy from the vendor directory) bootstrap, config, logs and public directory in your application's base directory.

```
$ cp -pr views.copy views
$ cp -pr config.copy config
$ cp -pr public.copy public
```

Make the logs directory writeable by the web server process owner

#### public

The public directory should contain an **index.php** file with the following:

**public/index.php**

~~~~
<?php
require __DIR__ . '/../bootstrap/autoload.php';
$app = require_once __DIR__ . '/../bootstrap/start.php';
~~~~

#### bootstrap

The bootstrap directory should contain the following:

**bootstrap/autoload.php**

~~~~
<?php
require __DIR__.'/../vendor/autoload.php';
~~~~

**bootstrap/start.php**

~~~~
<?php
//DI container
$container = new Pimple\Container();
//instantiate new instance of the app
$app = Smorken\Application\App::getInstance($container);

//add paths
$app->addPaths(require __DIR__ . '/paths.php');

//start it up: loads configs, bootstraps services, etc
$app->start();

define('DEBUG', $app['config']['app.debug']);

//authentication service logic can go here
//router service can go here

return $app;
~~~~

**bootstrap/paths.php**

~~~~
<?
return array(

/*
|--------------------------------------------------------------------------
| Public Path
|--------------------------------------------------------------------------
|
| The public path contains the assets for your web application, such as
| your JavaScript and CSS files, and also contains the primary entry
| point for web requests into these applications from the outside.
|
*/

'public' => __DIR__.'/../public',

/*
|--------------------------------------------------------------------------
| Base Path
|--------------------------------------------------------------------------
|
| The base path is the root of the installation. Most likely you
| will not need to change this value. But, if for some wild reason it
| is necessary you will do so here, just proceed with some caution.
|
*/

'base' => __DIR__.'/..',

);
~~~~

#### config

The config directory should contain the following:

**config/app.php**

~~~~
<?php
/**
 * App specific configuration items
 * Copy this file to your application's base/config
 * and modify for your use.
 * The following services are automatically bootstrapped:
 * 'Smorken\Config\ConfigService'
 * 'Smorken\Logger\LoggerService'
 * 'Smorken\Error\ErrorService'
 * 'Smorken\Session\SessionService'
 * 'Smorken\Http\UrlService'
 * TODO: decide whether to bootstrap explicitly or allow override method
 */
return array(
    'debug' => false,
    'services' => array(
        'Smorken\View\ViewService',
    ),
);
~~~~

**config/log.php**

~~~~
<?php

return array(
    'loggers' => array(
        'errors' => array(
            'handlers' => array(
                'Monolog\Handler\StreamHandler' => array(
                    \Smorken\Utils\PathUtils::base() . '/logs/errors.log',
                    Monolog\Logger::WARNING,
                ),
            ),
        ),
    )
);
~~~~

**config/views.php** (if you plan to use views)

~~~~
<?php
/**
 * 'provider' is the view renderer.  PhpView is a simple variable injector into a standard php file
 * 'loaderopts' represents the base path to the views
 */
return array(
    'provider' => 'Smorken\View\PhpView',
    'loaderopts' => array(\Smorken\Utils\PathUtils::base() . '/views'),
);
~~~~

### Routing
To use a router (currently there is only the LegacyRouter which turns the old every-endpoint-points-to-a-
file style into a single entry point (index.php) which can be used to route the old paths to new logic and 
views) add a service to **config/app.php**.  Poor man's front controller for legacy apps.

```'Smorken\Router\RouterService'```

Add a **routes** directory to your application base directory.

Add a file named **routes.php** into the routes directory.

Add the following after any extra logic in your **bootstrap/start.php** file (before ```return $app;```):

**bootstrap/start.php**

~~~~
...
$router = $app['router'];
if ($router) {
    require_once __DIR__ . '/../routes/routes.php';
    $router->dispatch($_SERVER['REQUEST_URI']);
}
return $app;
~~~~

Add a **.htaccess** file to the public directory - something like: 

**public/.htaccess**

~~~~
RewriteEngine On
RewriteBase /mybasepath/
# Redirect Trailing Slashes...
RewriteRule ^(.*)/$ /$1 [L,R=301]

# Handle Front Controller...
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^.*$ index.php [L]
~~~~

Add a route or two to start with to the **routes/routes.php** file:

**routes/routes.php**

~~~~
<?php
$routes = $app['routes'];

//root routes - handle both / and /index.php
$routes->get('', function($request) use ($app) {
    $app['view']->render('index');
});
$routes->get('index.php', function($request) use ($app) {
    $app['view']->render('index');
});
~~~~

For complex pages with business logic and presentation layer, you can require another file and have that file render
the view.

**routes/routes.php**

~~~~
$routes->get('', function($request) use ($app) {
    require __DIR__ . '/handlers/index.php';
});
~~~~

**routes/handlers/index.php**

~~~~
<?php
//logic goes here...
$app['view']->render[Partial]('index', array('myvar' => $something));
~~~~

**views/index.php**

~~~~
<div>Blah blah</div>
<p><?php echo $myvar; ?></p>
~~~~

Not remotely a true model-view-controller pattern, but a quick and dirty way to move a legacy app 
into something of a framework.