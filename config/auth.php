<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/23/14
 * Time: 11:03 AM
 */
return array(
    /*
    |--------------------------------------------------------------------------
    | Authentication Backend
    |--------------------------------------------------------------------------
    |
    | Provides the authentication backend (how do we find out if a user is valid)
    | and any options it needs to run
    |
    */
    'backend' => array(
        'class' => '\Smorken\Auth\Backend\DummyBackend',
        /*'class' => '\Smorken\Auth\Backend\LdapQueryBackend',
        'options' => [
            'ldap_provider' => 'LdapQuery\Opinionated\ActiveDirectory',
            'provider_options' => [
                'bind_filter' => '%s@domain.org',
                'host' => 'ldap.domain.org',
                'base_dn' => 'ou=group,dc=domain,dc=org',
                'bind_user' => 'user',
                'bind_password' => 'pw',
                'client_options' => [
                    'start_tls' => true,
                ],
            ],
            'load_groups' => false,
            'username_attribute' => 'samaccountname',
            'id_attribute' => 'mccdstudentid',
            'group_attribute' => 'memberOf',
            'maptouser' => [],
        ],*/
    ),
    /*
    |--------------------------------------------------------------------------
    | Login route
    |--------------------------------------------------------------------------
    |
    | What route to use for login
    |
    */
    'login_route' => 'login',
    /*
    |--------------------------------------------------------------------------
    | Return URL key
    |--------------------------------------------------------------------------
    |
    | The key to set in the session with the return URL
    |
    */
    'return_url_key' => 'returnUrl',
);
