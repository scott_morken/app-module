<?php
/**
 * App specific configuration items
 * Copy this file to your application's base/config
 * and modify for your use.
 * The following services are automatically bootstrapped:
 * 'Smorken\Config\ConfigService'
 * 'Smorken\Logger\LoggerService'
 * 'Smorken\Error\ErrorService'
 * 'Smorken\Session\SessionService'
 * 'Smorken\Http\UrlService'
 * TODO: decide whether to bootstrap explicitly or allow override method
 */
return array(
    /*
    |--------------------------------------------------------------------------
    | Debug
    |--------------------------------------------------------------------------
    |
    | True or false, this changes how the error handler service displays errors
    | If true, the error handler will dump the error to the screen, if false, it
    | will provide a generic message
    |
    */
    'debug' => true,
    /*
    |--------------------------------------------------------------------------
    | Services
    |--------------------------------------------------------------------------
    |
    | Additional services to register with Smorken\Application\App
    |
    */
    'services' => array(
        'Smorken\View\ViewService',
        'Smorken\Auth\AuthService',
        'Smorken\Router\RouterService',
    ),
);